package com.example.lunsreylish.homework9.ui.register.interator;

import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public interface RegisterInterator {

    void Register(User user, List<User> userList, RegisterResponceInterator registerResponceInterator);
    interface RegisterResponceInterator{
        void onSuccessRegister();
        void onErrorRegister();
        void onErrorUsername();
        void onErrorPassword();
    }
}
