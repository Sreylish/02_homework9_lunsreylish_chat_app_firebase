package com.example.lunsreylish.homework9.ui.login.presenter;

import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public interface LoginPresenter {
    interface LoginView{
        void onErrorUserNameLogin();
        void onErrorUserPasswordLogin();
        void onErrorLogin();
        void onSuccessLogin();
    }
    interface LoginUserPresenter{
        void onLogin(User user, List<User> userList);
    }
}

