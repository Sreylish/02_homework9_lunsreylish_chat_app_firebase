package com.example.lunsreylish.homework9.ui.login.presenter;

import com.example.lunsreylish.homework9.entity.User;
import com.example.lunsreylish.homework9.ui.login.interator.LoginInterator;
import com.example.lunsreylish.homework9.ui.login.interator.LoginInteratorImp;

import java.util.List;

public class LoginPresenterImp implements LoginPresenter.LoginUserPresenter,LoginInterator.LoginResponseInterator {
    private LoginInterator loginInterator;
    private LoginPresenter.LoginView loginView;

    public LoginPresenterImp( LoginPresenter.LoginView loginView) {
        this.loginInterator = new LoginInteratorImp();
        this.loginView = loginView;
    }


    @Override
    public void onSuccessLogin() {
        if(loginView!=null){
            loginView.onSuccessLogin();
        }
    }

    @Override
    public void onErrorLogin() {
        if(loginView!=null){
            loginView.onErrorLogin();
        }
    }

    @Override
    public void onErrorUsernameLogin() {
        if(loginView!=null){
            loginView.onErrorUserNameLogin();
        }
    }

    @Override
    public void onErrorPasswordLogin() {
        if(loginView!=null){
            loginView.onErrorUserPasswordLogin();
        }
    }

    @Override
    public void onLogin(User user, List<User> userList) {
        loginInterator.login(user,userList,this);
    }
}
