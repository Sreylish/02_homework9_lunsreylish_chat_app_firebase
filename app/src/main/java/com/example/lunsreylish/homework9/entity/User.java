package com.example.lunsreylish.homework9.entity;


import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private String userId;
    private String userName;
    private String userPassword;
    private String message;
    private long time;


    public User(){}

    public User(String userId, String userName, String userPassword, String message) {
        this.userId = userId;
        this.userName = userName;
        this.userPassword = userPassword;
        this.message = message;
    }

    protected User(Parcel in) {
        userId = in.readString();
        userName = in.readString();
        userPassword = in.readString();
        message = in.readString();
        time = in.readLong();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(userName);
        dest.writeString(userPassword);
        dest.writeString(message);
        dest.writeLong(time);
    }
}
