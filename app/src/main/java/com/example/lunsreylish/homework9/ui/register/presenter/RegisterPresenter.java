package com.example.lunsreylish.homework9.ui.register.presenter;

import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public interface RegisterPresenter {
    interface RegisterView{
        void onErrorUserName();
        void onErrorUserPassword();
        void onErrorRegister();
        void onSuccessRegister();
    }
    interface RegisterUserPresenter{
        void onRegister(User user, List<User> userList);
        void onDestroyView();
    }
}
