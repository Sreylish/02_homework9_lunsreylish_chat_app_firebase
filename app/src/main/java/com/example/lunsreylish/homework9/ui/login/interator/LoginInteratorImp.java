package com.example.lunsreylish.homework9.ui.login.interator;

import android.util.Log;

import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public class LoginInteratorImp implements LoginInterator {
    @Override
    public void login(User user, List<User> userList, LoginResponseInterator loginResponseInterator) {
        if(user.getUserName().isEmpty()){
            loginResponseInterator.onErrorUsernameLogin();
        }else if(user.getUserPassword().isEmpty()){
            loginResponseInterator.onErrorPasswordLogin();
    }else {
            boolean check = false;
            for (User user1 : userList) {
                Log.e("ooo1","Compare:"+user.getUserName()+"="+user1.getUserName()+","+user.getUserPassword()+"="+user1.getUserPassword());
                if (user.getUserName().equals(user1.getUserName()) && user.getUserPassword().equals(user1.getUserPassword())){
                    check = true;
                    Log.e("ooo1","True"+user.getUserName()+"="+user1.getUserName()+","+user.getUserPassword()+"="+user1.getUserPassword());
                    break;
                }

            }
            if (check == true) {
                Log.e("ooo1","true");
                loginResponseInterator.onSuccessLogin();
            } else {
                Log.e("ooo1","false");
                loginResponseInterator.onErrorLogin();
            }
        }
    }
}
