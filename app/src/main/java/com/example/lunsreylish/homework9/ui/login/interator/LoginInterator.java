package com.example.lunsreylish.homework9.ui.login.interator;

import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public interface LoginInterator {
    void login(User user, List<User> userList,LoginResponseInterator loginResponseInterator);
    interface LoginResponseInterator {
        void onSuccessLogin();
        void onErrorLogin();
        void onErrorUsernameLogin();
        void onErrorPasswordLogin();
    }
}
