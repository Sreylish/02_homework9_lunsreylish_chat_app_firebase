package com.example.lunsreylish.homework9.ui.register.presenter;

import com.example.lunsreylish.homework9.entity.User;
import com.example.lunsreylish.homework9.ui.register.interator.RegisterInteratorImpl;
import com.example.lunsreylish.homework9.ui.register.interator.RegisterInterator;

import java.util.List;

public class RegisterPresenterImp implements RegisterPresenter.RegisterUserPresenter, RegisterInterator.RegisterResponceInterator {
    private RegisterInterator registerInterator;
    private RegisterPresenter.RegisterView registerView;

    public RegisterPresenterImp(RegisterPresenter.RegisterView registerView) {
        this.registerView = registerView;
        this.registerInterator = new RegisterInteratorImpl();
    }

    @Override
    public void onRegister(User user, List<User> userList) {
        registerInterator.Register(user, userList, this);
    }

    @Override
    public void onSuccessRegister() {
        if (registerView != null) {
            registerView.onSuccessRegister();
        }
    }

    @Override
    public void onErrorRegister() {
        if (registerView != null) {
            registerView.onErrorRegister();
        }
    }

    @Override
    public void onErrorUsername() {
        if (registerView != null) {
            registerView.onErrorUserName();
        }
    }

    @Override
    public void onErrorPassword() {
        if (registerView != null) {
            registerView.onErrorUserPassword();
        }
    }

    @Override
    public void onDestroyView() {
        if (registerView != null) {
            registerView = null;
        }
    }
}
