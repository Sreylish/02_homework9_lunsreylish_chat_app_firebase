package com.example.lunsreylish.homework9.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lunsreylish.homework9.R;
import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    Context context;
    List<User> userList;
    User user;

    public ChatAdapter(List<User> userList,User user,Context context) {
        this.userList = userList;
        this.user=user;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_row,viewGroup,false);
        return new MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        int colorCurrentUser=context.getResources().getColor(R.color.colorPrimary);
        int colorOtherUser=context.getResources().getColor(R.color.colorAccent);

        RelativeLayout.LayoutParams paramName =(RelativeLayout.LayoutParams)myViewHolder.user.getLayoutParams();
        RelativeLayout.LayoutParams paramText =(RelativeLayout.LayoutParams)myViewHolder.cardView.getLayoutParams();

        if(user.getUserName().equals(userList.get(i).getUserName()) && user.getUserPassword().equals(userList.get(i).getUserPassword())) {
            myViewHolder.cardView.setCardBackgroundColor(colorCurrentUser);
            paramName.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            paramText.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }else{
            myViewHolder.cardView.setCardBackgroundColor(colorOtherUser);
            paramName.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            paramText.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        myViewHolder.user.setText(userList.get(i).getUserName());
        myViewHolder.text.setText(userList.get(i).getMessage());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView user,text;
        private CardView cardView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            user = itemView.findViewById(R.id.message_user);
            text = itemView.findViewById(R.id.message_string);
            cardView= itemView.findViewById(R.id.card_view);
        }
    }

}
