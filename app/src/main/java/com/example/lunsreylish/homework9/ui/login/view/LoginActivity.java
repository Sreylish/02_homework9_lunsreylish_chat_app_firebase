package com.example.lunsreylish.homework9.ui.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lunsreylish.homework9.ChatActivity;
import com.example.lunsreylish.homework9.R;
import com.example.lunsreylish.homework9.base.BaseActivity;
import com.example.lunsreylish.homework9.entity.User;
import com.example.lunsreylish.homework9.ui.login.presenter.LoginPresenter;
import com.example.lunsreylish.homework9.ui.login.presenter.LoginPresenterImp;
import com.example.lunsreylish.homework9.ui.register.view.MainActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends BaseActivity implements LoginPresenter.LoginView {

    List<User> users;
    TextView textView,textViewtoRegis;
    EditText uName,uPass;
    LoginPresenter.LoginUserPresenter loginUserPresenter;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        textView = findViewById(R.id.txtViewLogin);
        uName = findViewById(R.id.edtUserNameLogin);
        uPass = findViewById(R.id.edtUserPassLogin);
        Button btnLogin = findViewById(R.id.btnLogin);
        textViewtoRegis= findViewById(R.id.gotoRegister);
        textViewtoRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("user");
        loginUserPresenter = new LoginPresenterImp(this);
        users = new ArrayList<>();
        getAllUser();
        btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
            public void onClick(View v) {
                loginUserPresenter.onLogin(getLoginUser(),users);
            }
        });
    }

    public void getAllUser() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    User user = d.getValue(User.class);
                    users.add(user);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private User getLoginUser(){
        String name = uName.getText().toString();
        String password = uPass.getText().toString();
        User user = new User();
        user.setUserName(name);
        user.setUserPassword(password);
        return user;
    }

    @Override
    public void onErrorUserNameLogin() {
        Toast.makeText(this, "Error UserName", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorUserPasswordLogin() {
        Toast.makeText(this, "Error User Password", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorLogin() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSuccessLogin() {
        Intent intent = new Intent(LoginActivity.this, ChatActivity.class);
        intent.putExtra("username", (Parcelable) getLoginUser());
        startActivity(intent);
    }
}
