package com.example.lunsreylish.homework9;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lunsreylish.homework9.Adapter.ChatAdapter;
import com.example.lunsreylish.homework9.entity.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {
    FloatingActionButton sent;
    FirebaseDatabase firebaseDatabase;
    TextView message,userName;
    EditText msg;
    ChatAdapter adapter;
    List<User> userList;
    DatabaseReference databaseReference;
    RecyclerView recyclerView;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        userList=new ArrayList<>();
        message = findViewById(R.id.message_string);
        recyclerView = findViewById(R.id.recycler_view);
        msg= findViewById(R.id.input);
         user =  getIntent().getParcelableExtra("username");
        Log.e("hhhhh", "onCreate: "+user.getUserName() );
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("user");
        sent = findViewById(R.id.fab);
        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveMessage();
            }
        });
        getAllUser();
        setAdapter();
    }
    public void getAllUser() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    User user = d.getValue(User.class);
                    if (user.getMessage()!=null){
                        userList.add(user);
                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void setAdapter(){
        adapter=new ChatAdapter(userList,user,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount());
    }
    public void saveMessage(){
        User user1 = new User();
        String m= msg.getText().toString();
        user1.setMessage(m);
        user1.setUserName(user.getUserName());
        user1.setUserPassword(user.getUserPassword());
        String id = databaseReference.push().getKey();
        databaseReference.child(id).setValue(user1);
    }
}
