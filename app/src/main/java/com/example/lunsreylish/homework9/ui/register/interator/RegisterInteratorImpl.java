package com.example.lunsreylish.homework9.ui.register.interator;

import android.util.Log;

import com.example.lunsreylish.homework9.entity.User;

import java.util.List;

public class RegisterInteratorImpl implements RegisterInterator {
    @Override
    public void Register(User user, List<User> userList, RegisterResponceInterator registerResponceInterator) {
        if(user.getUserName().isEmpty()){
            registerResponceInterator.onErrorUsername();
        }else if ( user.getUserPassword().isEmpty()){
            registerResponceInterator.onErrorPassword();
        }else {
            Boolean check=false;
            for (User user1:userList) {
                if(user.getUserName().equals(user1.getUserName()) && user.getUserPassword().equals(user1.getUserPassword())){
                    Log.e(" oooo1","Check true" );
                    check=true;
                    break;
                }
            }
            if(check==true){
                registerResponceInterator.onErrorRegister();
            }else{
                Log.e(" oooo1","Check false" );
                registerResponceInterator.onSuccessRegister();
            }
        }
    }

}
