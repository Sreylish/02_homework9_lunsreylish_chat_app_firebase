package com.example.lunsreylish.homework9.ui.register.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lunsreylish.homework9.R;
import com.example.lunsreylish.homework9.entity.User;
import com.example.lunsreylish.homework9.ui.login.view.LoginActivity;
import com.example.lunsreylish.homework9.ui.register.presenter.RegisterPresenter;
import com.example.lunsreylish.homework9.ui.register.presenter.RegisterPresenterImp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RegisterPresenter.RegisterView {

    RegisterPresenter.RegisterUserPresenter registerPresenter;

    TextView textView,textViewtoLogin;
    EditText uName, uPass;
    Button btnRegister;
    List<User> users;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.txtViewRegister);
        uName = findViewById(R.id.edtUserName);
        uPass = findViewById(R.id.edtUserPass);
        btnRegister = findViewById(R.id.btnRegister);
        textViewtoLogin = findViewById(R.id.gotoLogin);
        textViewtoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });
        users = new ArrayList<>();
        registerPresenter = new RegisterPresenterImp(this);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("user");
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerPresenter.onRegister(getUser(), users);
            }
        });
        getAllUser();
    }
    public void getAllUser() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    User user = d.getValue(User.class);
                    users.add(user);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private User getUser(){
        String name = uName.getText().toString();
        String password = uPass.getText().toString();
        User user = new User();
        user.setUserName(name);
        user.setUserPassword(password);
        return user;
    }

    private void registerUser() {
        String id = databaseReference.push().getKey();
        databaseReference.child(id).setValue(getUser());
    }

    @Override
    public void onErrorUserName() {
        uName.setError("Error Username");
    }

    @Override
    public void onErrorUserPassword() {
       uPass.setError("Error User Password");
    }

    @Override
    public void onErrorRegister() {
        Toast.makeText(this, "This account already register.Please Login", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSuccessRegister() {
        registerUser();
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        registerPresenter.onDestroyView();
    }
}
